-module(ts).
-export([new/0, out/2, in/2]).
    
new() ->
    spawn_link(fun() -> server([], []) end).

out(TS, Tuple) ->
    TS ! {post, Tuple}.

in(TS, Pattern) ->
    Host = self(),
    Ref = make_ref(),
	TS ! {get, Ref, Host, Pattern},
    receive
        {Ref, MatchingTuple} ->
            MatchingTuple
    end.

server(TupleList, Requests) ->
	receive
		{get, Ref, From, Pattern} -> % Get from TupleSpace 
			case take(Pattern, TupleList, []) of 
				notFound -> 
					server(TupleList, Requests ++ [{Ref, From, Pattern}]);

				{found, MatchingTuple, NewTupleList} ->
					From ! {Ref, MatchingTuple},
					server(NewTupleList, Requests)		
			end;
		
		{post, Tuple} -> % Post to TupleSpace
            %io:format("POST: ~w~n", [Tuple]),
			case serveOldRequests(Requests, TupleList ++ [Tuple], []) of
				noMatch -> 
					server(TupleList ++ [Tuple], Requests);
				{match, NewRequests, NewTupleList} ->
					server(NewTupleList, NewRequests)
			end;
		_ ->
			io:format("Bad argument~n"),
			server(TupleList, Requests)
	end.
serveOldRequests([], _, _) ->
	noMatch;
serveOldRequests(Requests, TupleList, CheckedRequests) ->
	[Req | Reqs]    = Requests, % For readability
	{Ref, From, Pattern} = Req,
	case take(Pattern, TupleList, []) of
		notFound ->
			serveOldRequests(Reqs, TupleList, CheckedRequests ++ [Req]);
		{found, MatchingTuple, NewTupleList} ->
			From ! {Ref, MatchingTuple},
			{match, CheckedRequests ++ Reqs, NewTupleList}
	end.


take(_, [], _) -> 
	%io:format("Pattern not found~n"),
	notFound;

take(Pattern, [X | XS], Checked) ->
	case match(Pattern, X) of
		false -> 
			take(Pattern, XS, Checked ++ [X]);
		true -> 
		       {found, X, Checked ++ XS}
	end.	

match(any, _) -> true;

match(P,Q) when is_tuple(P),
		is_tuple(Q)
		-> match(tuple_to_list(P), tuple_to_list(Q));

match([P|PS], [L|LS]) -> case match(P,L) of
				 true -> match(PS,LS);
				 false -> false
			 end;

match(P,P) -> true;

match(_,_) -> false.
