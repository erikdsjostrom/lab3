-module(ourTests).
-import(ts, [in/2, out/2, new/0]).
-export[(start_test/0)].

start_test() ->
    % Spawns two new servers with an empty tuplespace
    Server1 = new(),
    Server2 = new(),
   
    % Spawns a process inserting {karl, name} into the
    % tuplespace in Server1 
    spawn_link(fun() -> out(Server1, {karl, namn}) end),
    sleep(200),
    
    % Spawns a process inserting {albin, name} into the
    % tuplespace in Server2 
    spawn_link(fun() -> out(Server2, {albin, namn}) end),
    sleep(200),
    
    % Tries to find a tuple matching the pattern {albin, name} in Server1 
    % should fail because there is such tuple in Server 1
    %test_in(Server1, {albin, namn}),
    spawn_link(fun() -> test_in(Server2, {albin, namn}) end),
    sleep(200),
   
    % tries to find a truple matching the same pattern in Server2
    % Should work since there is such a tuple in Server2
    spawn_link(fun() -> test_in(Server2, {albin, namn}) end),
    sleep(200),
    
    % tries to find a truple matching the pattern {karl, namn} in Server2
    % Should work since there is such a tuple in Server2
    spawn_link(fun() -> test_in(Server2, {karl, namn}) end),
    sleep(200),
    
    % We can still print stuff since the process is blocked!
    io:format("The process is blocked!~n"),
    sleep(200),
    
    % If we insert a matching tuple the process resumes
    spawn_link(fun() -> out(Server2, {karl, namn}) end),
    sleep(200),
    
    % tries to find a truple matching the same pattern in Server2
    % should fail since we have already extracted that tuple
    spawn_link(fun() -> test_in(Server2, {karl, namn}) end).

test_in(TS, Pattern) ->
    Pid = self(),
    spawn_link(fun() -> new_in(Pid, TS, Pattern) end),
    receive
        Match ->
            io:format("Found a tuple matching pattern: ~w~n", [Pattern])
    after
        1000 ->
            io:format("Found no tuple matching pattern: ~w~n", [Pattern])
    end.


new_in(Pid, TS, Pattern) ->
    % Spawns a new in() process which checks if there exists
    % a tuple matching the pattern in the tuplespace TS
    case in(TS, Pattern) of
        R ->
            Pid ! R
    end.

sleep(T) ->
    receive
    after
        T -> 
            true
    end.



